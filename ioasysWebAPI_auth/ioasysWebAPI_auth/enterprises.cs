namespace ioasysWebAPI_auth
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class enterprises
    {
        public int id { get; set; }

        [StringLength(50)]
        public string email_enterprise { get; set; }

        [StringLength(100)]
        public string facebook { get; set; }

        [StringLength(100)]
        public string twitter { get; set; }

        [StringLength(100)]
        public string linkedin { get; set; }

        [StringLength(50)]
        public string phone { get; set; }

        public bool? own_enterprise { get; set; }

        [StringLength(50)]
        public string enterprise_name { get; set; }

        [StringLength(150)]
        public string photo { get; set; }

        public string description { get; set; }

        [StringLength(50)]
        public string city { get; set; }

        [StringLength(50)]
        public string country { get; set; }

        public int? value { get; set; }

        public int? share_price { get; set; }

        public int? enterprise_type { get; set; }
    }
}
